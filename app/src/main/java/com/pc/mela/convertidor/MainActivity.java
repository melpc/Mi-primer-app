package com.pc.mela.convertidor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    TextView txtC, txtF;
    SeekBar seekBar;
    double c, f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtC= findViewById(R.id.txtC);
        txtF= findViewById(R.id.txtF);

        seekBar= findViewById(R.id.seekBar2);
        seekBar.setMax(400);
        seekBar.setProgress(200);

        c = seekBar.getProgress()-200;
        f = c*1.8+32;
        txtC.setText(String.format(Locale.getDefault(), "%.1f", c));
        txtF.setText(String.format(Locale.getDefault(), "%.1f", f));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                c = progress-200;
                f = c*1.8+32;
                txtC.setText(String.format(Locale.getDefault(), "%.1f", c));
                txtF.setText(String.format(Locale.getDefault(), "%.1f", f));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        Log.i("Convertidor", "Melany Denisse Patiño Chancay");
    }
}
